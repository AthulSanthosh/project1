// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"

// create a function which returns the last car details
function last_car(carlist) {
    // check if the car list is empty or not , if it is thow an error
    if (carlist === undefined || carlist.length === 0)
    {
        console.log([])
        return false
    }

    last_car_length = carlist.length - 1
    return carlist[last_car_length]

}
module.exports = last_car;