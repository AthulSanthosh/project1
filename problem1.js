// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
// function to find the car id with 33
function car_id(carlist,c_id = 33) {



    // if it is an empty directory

    if (carlist === undefined || carlist.length === 0)
    {
    console.log([])
    return false
    }
    min_val = 0 
    max_val = carlist.length -1

    // if the value of i  is not between min_val  and max_val , throw an error msg and break
    if (c_id <=0 || c_id > max_val)
    {
        console.log("Invalid id")
        return false
    }
    for(i = 1; i <= carlist.length; i++)

    {
        if (carlist[i].id === c_id)
        return carlist[i]

    }
}

module.exports = car_id;

