// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function bmw_audi_only(car_list) {
if (!car_list){
    console.log([])
    return false
}

    bmw_audi = []
    for(i = 0; i<car_list.length;i++)
        if (car_list[i].car_make === "BMW" || car_list[i].car_make === "Audi")
        {
            bmw_audi.push(car_list[i])
        }
    return bmw_audi
}

module.exports = bmw_audi_only;
