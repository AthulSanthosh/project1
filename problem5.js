// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function older2000(car_year)
{   
    if (!car_year){
        console.log([])
        return false
    }

    older = []
    for (i = 0 ; i < car_year.length; i++) 
    
        if (car_year[i] < 2000)
            older.push(car_year[i])
    
    return older
}
module.exports = older2000;
