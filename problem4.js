// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function years(car_list) {
    if (!car_list){
        console.log([])
        return false
    }
    car_years = []
    for(i =0; i < car_list.length; i++)
    {
        car_years.push(car_list[i].car_year)
    }
    return car_years
}
module.exports = years;