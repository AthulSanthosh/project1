// importing the carlist from inventory
const inventory = require('./project1')
// import the last car function
const last_car = require('./problem2.js')

const last_car_data = last_car(inventory)
if (last_car_data !== false)
    console.log(`Last car is a ${last_car_data.car_make} ${last_car_data.car_model}`)
