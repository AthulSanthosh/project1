// import the inventory (carlist) module
const inventory = require('./project1.js')
// import the function for problem1 
let problem1 = require("./problem1")
// include the id as the second parameter to the function, 
// else function will take the default value as 33
id = 28
//const result = problem1(inventory,id)
const result = problem1([])
if (result !== false)
    {
    console.log(`Car id ${id} is a ${result.car_year} ${result.car_make} ${result.car_model}`)
    }