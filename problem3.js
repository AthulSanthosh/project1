// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.



function alpha_sorted(carlist) {
    if (!carlist){
        console.log("Empty inventory")
        return false
    }


    car_name = []
    // push all the car models to the array
    for(i = 0; i < carlist.length; i++)
    {
        car_name.push(carlist[i].car_model)

    }
    // sort the array
    car_name.sort()
    return car_name
}
module.exports = alpha_sorted;