// import the car list from inventory
const carlist = require('./project1')

// import the sort function which returns the sorted car array
const alpha_sorted = require('./problem3.js')


const alpha_sorted_car = alpha_sorted(carlist)

// print all the cars to console
if (alpha_sorted_car)
{
    console.log(alpha_sorted_car)
}