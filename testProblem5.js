// import the car_years from problem 4
const complete_years = require('./problem4')

const older2000 = require('./problem5')
// retrieve the car_list
const inventory = require('./project1')

// get all the years from problem 4
const car_years = complete_years(inventory)
// get all the years older than 2000

const older_than_2000 = older2000(car_years)

if (older_than_2000){
    console.log(older_than_2000)
    console.log(`there are ${older_than_2000.length} cars older than 2000 `)

}

